FROM fedora:36

RUN dnf update -y && dnf install -y openssh-server openssh-clients sshpass ansible 
RUN echo "===> Adding hosts for convenience..."        && \
    mkdir -p /etc/ansible                              && \
    echo 'localhost' > /etc/ansible/hosts


# default command: display Ansible version
CMD [ "ansible-playbook", "--version" ]
